<?php
/**
 * @file
 * uw_ct_feds_event_listing_header.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_event_listing_header_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: views_view
  $overrides["views_view.feds_events.display|block_feds_event_header|display_options|fields|title_field"] = array(
    'id' => 'title_field',
    'table' => 'field_data_title_field',
    'field' => 'title_field',
    'label' => '',
    'exclude' => TRUE,
    'element_label_colon' => FALSE,
    'link_to_entity' => 0,
  );

 return $overrides;
}
