<?php
/**
 * @file
 * uw_ct_feds_event_listing_header.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_event_listing_header_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_ct_feds_event_listing_header_views_default_views_alter(&$data) {
  if (isset($data['feds_events'])) {
    $data['feds_events']->display['block_feds_event_header']->display_options['fields']['title_field'] = array(
      'id' => 'title_field',
      'table' => 'field_data_title_field',
      'field' => 'title_field',
      'label' => '',
      'exclude' => TRUE,
      'element_label_colon' => FALSE,
      'link_to_entity' => 0,
    ); /* WAS: '' */
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_event_listing_header_node_info() {
  $items = array(
    'feds_event_listing_header' => array(
      'name' => t('Feds event listing header'),
      'base' => 'node_content',
      'description' => t('This header will be placed above the events listing page. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_feds_event_listing_header_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: comment_node_feds_event_listing_header
  $schemaorg['comment']['comment_node_feds_event_listing_header'] = array(
    'rdftype' => array(
      0 => 'sioc:Post',
      1 => 'sioct:Comment',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'comment_body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'pid' => array(
      'predicates' => array(
        0 => 'sioc:reply_of',
      ),
      'type' => 'rel',
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
  );

  // Exported RDF mapping: feds_event_listing_header
  $schemaorg['node']['feds_event_listing_header'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  );

  return $schemaorg;
}
