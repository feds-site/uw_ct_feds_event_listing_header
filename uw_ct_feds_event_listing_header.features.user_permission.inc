<?php
/**
 * @file
 * uw_ct_feds_event_listing_header.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_event_listing_header_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_event_listing_header content'.
  $permissions['create feds_event_listing_header content'] = array(
    'name' => 'create feds_event_listing_header content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_event_listing_header content'.
  $permissions['delete any feds_event_listing_header content'] = array(
    'name' => 'delete any feds_event_listing_header content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_event_listing_header content'.
  $permissions['delete own feds_event_listing_header content'] = array(
    'name' => 'delete own feds_event_listing_header content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_event_listing_header content'.
  $permissions['edit any feds_event_listing_header content'] = array(
    'name' => 'edit any feds_event_listing_header content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_event_listing_header content'.
  $permissions['edit own feds_event_listing_header content'] = array(
    'name' => 'edit own feds_event_listing_header content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_event_listing_header revision log entry'.
  $permissions['enter feds_event_listing_header revision log entry'] = array(
    'name' => 'enter feds_event_listing_header revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_event_listing_header authored by option'.
  $permissions['override feds_event_listing_header authored by option'] = array(
    'name' => 'override feds_event_listing_header authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_event_listing_header authored on option'.
  $permissions['override feds_event_listing_header authored on option'] = array(
    'name' => 'override feds_event_listing_header authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_event_listing_header promote to front page option'.
  $permissions['override feds_event_listing_header promote to front page option'] = array(
    'name' => 'override feds_event_listing_header promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_event_listing_header published option'.
  $permissions['override feds_event_listing_header published option'] = array(
    'name' => 'override feds_event_listing_header published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_event_listing_header revision option'.
  $permissions['override feds_event_listing_header revision option'] = array(
    'name' => 'override feds_event_listing_header revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_event_listing_header sticky option'.
  $permissions['override feds_event_listing_header sticky option'] = array(
    'name' => 'override feds_event_listing_header sticky option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_event_listing_header content'.
  $permissions['search feds_event_listing_header content'] = array(
    'name' => 'search feds_event_listing_header content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
